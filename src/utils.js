export const formatDate = (date) => date.toISOString().slice(0, 10) // "2021-05-22"

export const formatDateDisplay = (date) => {
  if (date) {
    return new Date(date).toLocaleDateString('en-GB', { day: 'numeric', month: 'long', year: 'numeric' }).toLowerCase() + '' // 22 may 2021
  }
}

export const formatDateDailyWeather = (date) => new Date(date * 1000).toLocaleDateString('en-GB', { day: 'numeric', month: 'long', year: 'numeric' }).toLowerCase() + '' // 22 may 2021

export const formatDateInDisplayFormat = (date) => new Date(date).toLocaleDateString().split('.').join('/') // 21/05/2021
