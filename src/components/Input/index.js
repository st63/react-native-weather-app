import React from 'react'
import { View } from 'react-native';
import { DatePicker } from 'react-native-woodpicker';

import { formatDate } from '../../utils'

export const DateInput = ({ handleText, pickedDate, setPickedDate, color }) => {

  const today = new Date()

  const minDate = formatDate(new Date(new Date().setDate(today.getDate() - 5)))
  const maxDate = formatDate(today)

  return (
    <View style={{ height: 50 }}>
      <DatePicker
        value={pickedDate}
        onDateChange={setPickedDate}
        title="Select date"
        text={handleText()}
        isNullable
        style={{
          borderWidth: 2,
          borderColor: "rgba(128, 131, 164, 0.2)",
          borderRadius: 8,
          width: 280,
          height: 48,
          marginTop: 24,
          padding: 10
        }}
        textInputStyle={{
          color: color,
          fontSize: 16
        }}
        minDate={minDate}
        maxDate={maxDate}
        locale="ru"
      />
    </View>
  )
}