import React, { useState, useEffect } from 'react';
import { Text, View, Image, ScrollView } from 'react-native';

import { DateInput } from '../Input/index';
import { CitySelect } from '../CitySelect';
import { WeatherCard } from '../WeatherCard';
import { getWeatherOfDay } from '../../../api/index';
import { formatDateInDisplayFormat } from '../../utils';
import { styles } from './styles';
import placeholderIcon from '../../../assets/placeholder-icon.png';

export const WeatherBlockOfThePast = () => {
  const [value, selectValue] = useState("Select city")
  const [city, selectCity] = useState("Select city")
  const [pickedDate, setPickedDate] = useState();
  const [weatherOnThePast, setWeatherOnThePast] = useState()

  useEffect(() => {
    getWeatherOfDay(value, pickedDate).then((weather) => setWeatherOnThePast(weather))
  }, [city, pickedDate])

  const handleText = () => pickedDate
    ? formatDateInDisplayFormat(pickedDate)
    : "Select date";

  return (
    <View style={styles.weatherBlock}>
      <Text style={styles.weatherTitle}>Forecast for a Date in the Past</Text>
      <CitySelect
        value={value}
        city={city}
        selectValue={selectValue}
        selectCity={selectCity}
        color={city !== "Select city" ? '#2C2D76' : '#8083A4'}
        marginRight={0}
      />
      <DateInput
        handleText={handleText}
        pickedDate={pickedDate}
        setPickedDate={setPickedDate}
        color={pickedDate ? '#2C2D76' : '#8083A4'}
      />
      {weatherOnThePast
        ? (
          <View style={styles.weatherContentWrapper}>
            <ScrollView style={styles.weatherScrollWrapper} horizontal={true}>
              <WeatherCard
                key={weatherOnThePast.date}
                date={weatherOnThePast.date}
                icon={weatherOnThePast.icon}
                temp={weatherOnThePast.temp}
                width={275}
                marginLeft={14}
              />
            </ScrollView>
          </View>
        )
        : (
          <View style={styles.emptyWeatherBlock}>
            <Image style={styles.emptyWeatherIcon} source={placeholderIcon} />
            <Text style={styles.emptyWeatherPlaceholder}>Fill in all the fields and the weather will be displayed</Text>
          </View>
        )
      }
    </View>
  )
}