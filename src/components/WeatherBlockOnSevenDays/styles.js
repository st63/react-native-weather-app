import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  weatherBlock: {
    backgroundColor: '#fff',
    alignItems: "center",
    paddingTop: 32,
    paddingLeft: 24,
    paddingBottom: 63,
    borderRadius: 8,
    marginBottom: 10,
  },
  weatherContentWrapper: {
    height: 300,
  },
  weatherScrollWrapper: {
    display: 'flex',
    flexDirection: 'row',
  },
  emptyWeatherBlock: {
    fontWeight: 'bold',
    marginTop: 50,
  },
  weatherTitle: {
    marginBottom: 22,
    color: '#2C2D76',
    fontWeight: 'bold',
    fontSize: 32,
    lineHeight: 32,
    alignSelf: 'flex-start'
  },
  emptyWeatherIcon: {
    marginBottom: 22,
    width: 150,
    height: 150,
    alignSelf: 'center'
  },
  emptyWeatherPlaceholder: {
    color: '#8083A4',
    textAlign: 'center',
  },
})