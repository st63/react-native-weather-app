import React, { useState, useEffect } from 'react';
import { Text, View, Image, ScrollView } from 'react-native';

import { CitySelect } from '../CitySelect';
import { getWeather } from '../../../api/index';
import { WeatherCard } from '../WeatherCard';
import { styles } from './styles';
import placeholderIcon from '../../../assets/placeholder-icon.png';

const renderWeather = (day) => (
  <WeatherCard
    key={day.date}
    date={day.date}
    icon={day.icon}
    temp={day.temp}
    width={174}
    marginLeft={5}
  />
)

export const WeatherBlockOnSevenDays = () => {
  const [value, selectValue] = useState("Select city")
  const [city, selectCity] = useState("Select city")
  const [weatherOn7days, setWeatherOn7days] = useState()

  useEffect(() => {
    getWeather(value).then((weather => setWeatherOn7days(weather)))
  }, [city])

  return (
    <View style={styles.weatherBlock}>
      <Text style={styles.weatherTitle}>7 Days Forecast</Text>
      <CitySelect
        value={value}
        city={city}
        selectValue={selectValue}
        selectCity={selectCity}
        color={city !== "Select city" ? '#2C2D76' : '#8083A4'}
        marginRight={24}
      />
      {weatherOn7days
        ? (
          <View style={styles.weatherContentWrapper}>
            <ScrollView style={styles.weatherScrollWrapper} horizontal={true}>
              {weatherOn7days.map(renderWeather)}
            </ScrollView>
          </View>
        )
        : (
          <View style={styles.emptyWeatherBlock}>
            <Image style={styles.emptyWeatherIcon} source={placeholderIcon} />
            <Text style={styles.emptyWeatherPlaceholder}>Fill in all the fields and the weather will be displayed</Text>
          </View>
        )
      }
    </View>
  )
}