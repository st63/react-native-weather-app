import React from 'react'
import { View, Image, Text } from 'react-native'

import { styles } from './styles';

export const WeatherCard = ({ date, icon, temp, width, marginLeft }) => {
  const sign = (temp > 0) ? '+' : ''

  return (
    <View style={{
      backgroundColor: '#373AF5',
      borderRadius: 8,
      borderColor: '#2C2D76',
      borderStyle: 'solid',
      borderWidth: 2,
      height: 237,
      width: width,
      justifyContent: 'space-between',
      paddingTop: 20,
      paddingRight: 21,
      paddingBottom: 24,
      paddingLeft: 19,
      marginTop: 55,
      marginLeft: marginLeft,
      marginRight: 16,
    }}>
      <Text style={styles.date}>{date}</Text>
      <Image
        style={styles.icon}
        source={{
          uri: `https://openweathermap.org/img/wn/${icon}@4x.png`,
        }} />
      <Text style={styles.temp}>{sign}{temp}°</Text>
    </View>
  )
}