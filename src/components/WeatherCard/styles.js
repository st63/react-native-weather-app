import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  weatherCardInnerOfThePast: {
    alignItems: 'stretch',
  },
  date: {
    fontWeight: '700',
    textTransform: 'lowercase',
    color: '#fff',
  },
  icon: {
    height: 115,
    width: 115,
    alignSelf: 'center',
  },
  temp: {
    fontSize: 32,
    fontWeight: '700',
    alignSelf: 'flex-end',
    color: '#fff',
  }
})