import React from 'react'
import { Select, Option } from "react-native-chooser";

import { CITIES } from '../../config'

const renderCity = ({ value, name }) => <Option key={value} value={value}>{name}</Option>

export const CitySelect = ({ value, city, selectValue, selectCity, marginRight, color }) => {
  return (
    <Select
      onSelect={(value, name) => {
        selectValue(value)
        selectCity(name)
      }}
      defaultText={city}
      style={{
        borderWidth: 2,
        borderColor: "rgba(128, 131, 164, 0.2)",
        borderRadius: 8,
        width: 280,
        height: 48,
        marginRight: marginRight
      }}
      textStyle={{
        color: color,
        marginTop: 4,
        fontSize: 16
      }}
      backdropStyle={{ backgroundColor: "#d3d5d6" }}
      optionListStyle={{
        backgroundColor: "#fff",
        color: '#2C2D76'
      }}
    >
      {CITIES.map(renderCity)}
    </Select>
  )
}