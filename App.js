import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, ImageBackground, ScrollView } from 'react-native';
import backgroundTop from './assets/background-top.png';
import backgroundBottom from './assets/background-bottom.png';
import { WeatherBlockOfThePast } from './src/components/WeatherBlockOfThePast';
import { WeatherBlockOnSevenDays } from './src/components/WeatherBlockOnSevenDays';

export default function App() {
  return (
    <ScrollView style={styles.appBody}>
      <ImageBackground source={backgroundTop} style={styles.backgroundImage}>
        <ImageBackground source={backgroundBottom} style={styles.backgroundImage}>
          <View style={styles.container}>
            <View style={styles.header}>
              <Text style={styles.pageTitleWeather}>Weather</Text>
              <Text style={styles.pageTitleForecast}>forecast</Text>
            </View>
            <View style={styles.main}>
              <WeatherBlockOnSevenDays />
              <WeatherBlockOfThePast />
            </View>
            <View style={styles.footer}>
              <Text style={styles.footerContent}>C ЛЮБОВЬЮ ОТ MERCURY DEVELOPMENT</Text>
            </View>
            <StatusBar style="auto" />
          </View>
        </ImageBackground>
      </ImageBackground>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  appBody: {
    backgroundColor: '#373AF5',
    flex: 1,
  },
  backgroundImage: {
    flex: 1,
    width: '100%'
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: "space-between",
    paddingTop: 32,
    paddingBottom: 20,
    paddingHorizontal: 10,
  },
  header: {
    marginBottom: 24,
    width: 200,
  },
  pageTitleWeather: {
    textAlign: "left",
    lineHeight: 32,
    fontSize: 32,
    fontWeight: 'bold',
    color: '#fff',
  },
  pageTitleForecast: {
    textAlign: "right",
    fontSize: 32,
    lineHeight: 32,
    fontWeight: 'bold',
    color: '#fff',
  },
  footerContent: {
    color: '#fff',
    opacity: 0.6,
    marginTop: 68,
    lineHeight: 18,
    fontSize: 14,
  },
});

