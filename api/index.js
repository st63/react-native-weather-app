import axios from 'axios'
import { APP_ID } from '../src/config'
import { formatDateDailyWeather } from '../src/utils'

const mapDailyWeather = (day) => ({
  date: formatDateDailyWeather(day.dt),
  temp: Math.round(day.temp.day),
  icon: day.weather[0].icon
})

export const getWeather = async (city) => {
  const { data } = await axios.get(`https://api.openweathermap.org/data/2.5/onecall?${city}&exclude=current,minutely,hourly,alerts&units=metric&appid=${APP_ID}`)

  return data.daily.map(mapDailyWeather)
}


export const getWeatherOfDay = async (city, date) => {
  const formattedDay = new Date(date).getTime() / 1000
  const { data } = await axios(`https://api.openweathermap.org/data/2.5/onecall/timemachine?${city}&dt=${formattedDay}&units=metric&appid=${APP_ID}`)

  return {
    date: formatDateDailyWeather(data.current.dt),
    temp: Math.round(data.current.temp),
    icon: data.current.weather[0].icon,
  }
}